package pl.wspa.java.utils;

import java.util.Scanner;

public final class Utils {

    public static void p(Object o) {
        System.out.print(o);
    }

    public static String inString(String atrybut) {
        return in(atrybut, 'S');
    }

    public static int inInt(String atrybut) {
        return Integer.parseInt(in(atrybut, 'I'));
    }

    public static double inDouble(String atrybut) {
        return Double.parseDouble(in(atrybut, 'D'));
    }

    public static String in(String atrybut, char format) {

        Scanner sc = new Scanner(System.in);

        String wartoscAtrybutu;

        try {

            p(atrybut + ": ");

            switch (format) {
                case 'I':
                    while (!sc.hasNextInt()) {
                        p("Wprowadź poprawną liczbę całkowitą!");
                        sc.nextInt();
                    }
                    wartoscAtrybutu = sc.nextInt() + "";
                    break;
                case 'D':
                    while (!sc.hasNextDouble()) {
                        p("Wprowadź poprawną liczbę zmiennoprzecinkową!");
                        sc.nextDouble();
                    }
                    wartoscAtrybutu = sc.nextDouble() + "";
                    break;
                default:
                    while (!sc.hasNext()) {
                        p("Wprowadź poprawną wartość tekstu!");
                        sc.next();
                    }
                    wartoscAtrybutu = sc.next();
            }

            return wartoscAtrybutu;

        } catch (Exception e) {
            p("Błąd odczytu!");
            System.exit(-1);
        }
        return null;
    }

;

}
