package pl.wspa.java.lab.lab8.cw1.punkt4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyDerbyConnection {

    static Connection getConnection() throws SQLException {

        // • Rejestracja sterownika JDBC
        String driverClass = "org.apache.derby.jdbc.ClientDriver";
        try {
            Class.forName(driverClass);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CustomersReader.class.getName())
                    .log(Level.SEVERE,
                            "Błąd ładowania sterownika",
                            ex);
        }

        // • zdefiniowanie adresu URL połączenia
        String driverName = "derby";
        String host = "localhost";
        String port = "1527";
        String dbName = "sample";

        String databaseUrl
                = "jdbc:" + driverName + "://" + host + ":" + port + "/" + dbName;

        // • nawiązanie połączenia
        String username = "app";
        String password = "app";
        Connection conn = DriverManager.getConnection(
                databaseUrl,
                username,
                password);

        return conn;
    }
}
