package pl.wspa.java.lab.lab8.cw1.punkt4;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomersReader {

    public static CustomerDto[] getCustomers(String where) {

        try {

            // • nawiązanie połączenia
            Connection conn = MyDerbyConnection.getConnection();

            Statement stmt;
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            // • wykonanie zapytania lub aktualizacja danych
            String sql = "SELECT * FROM customer " + where;
            ResultSet rs = stmt.executeQuery(sql);

            rs.last();
            int rowCount = rs.getRow();

            rs = stmt.executeQuery(sql);
            CustomerDto[] customers = new CustomerDto[rowCount];
            int i = 0;
            // • przetworzenie wyników
            while (rs.next()) {
                int customerId = rs.getInt("CUSTOMER_ID");
                String discountCode = rs.getString("DISCOUNT_CODE");
                String zip = rs.getString("ZIP");
                String name = rs.getString("NAME");
                String city = rs.getString("CITY");
                String eMail = rs.getString("EMAIL");
                int creditLimit = rs.getInt("CREDIT_LIMIT");

                customers[i] = CustomerDto.create(customerId, discountCode, zip, name, city, eMail, creditLimit);
                i++;

            }

            // • zamknięcie połączenia
            conn.close();

            return customers;

        } catch (SQLException ex) {

            Logger.getLogger(CustomersReader.class.getName())
                    .log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public static void main(String[] args) {
        for (CustomerDto c : getCustomers("")) {
            System.out.println(c.getCustomerId() + ";"
                    + c.getDiscountCode() + ";"
                    + c.getName() + ";"
                    + c.getCity() + ";"
                    + c.getZip() + ";"
                    + c.geteMail() + ";"
                    + c.getCreditLimit()
            );
        };
    }

}
