package pl.wspa.java.lab.lab8.cw1.punkt4;

public class CustomerDto {

    private final int customerId;
    private final String discountCode;
    private final String zip;
    private final String name;
    private final String city;
    private final String eMail;
    private final int creditLimit;

    public static CustomerDto create(int customerId, String discountCode, String zip, String name, String city, String eMail, int creditLimit) {
        return new CustomerDto(customerId, discountCode, zip, name, city, eMail, creditLimit);
    }

    private CustomerDto(int customerId, String discountCode, String zip, String name, String city, String eMail, int creditLimit) {
        this.customerId = customerId;
        this.discountCode = discountCode;
        this.zip = zip;
        this.name = name;
        this.city = city;
        this.eMail = eMail;
        this.creditLimit = creditLimit;
    }

    /**
     * @return the customerId
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * @return the discountCode
     */
    public String getDiscountCode() {
        return discountCode;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the eMail
     */
    public String geteMail() {
        return eMail;
    }

    /**
     * @return the creditLimit
     */
    public int getCreditLimit() {
        return creditLimit;
    }

}
