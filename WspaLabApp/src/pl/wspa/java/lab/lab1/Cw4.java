package pl.wspa.java.lab.lab1;

public class Cw4 {

    public static void main(String[] args) {
        a();
        //b();
        //c();
    }

    static void a() {
        int n = 4;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                o("*");
            }
            o("\n");
        }
    }

    static void b() {
        int n = 4;
        String znak,
                znakZewnetrzny = "*",
                znakWewnetrzny = "#",
                znakNL = "\n";

        for (int y = 0; y < n; y++) {
            for (int x = 0; x < n; x++) {
                znak = znakZewnetrzny;
                if (y > 0 && y < n - 1) {
                    if (x > 0 && x < n - 1) {
                        znak = znakWewnetrzny;
                    }
                }
                o(znak);

            }
            o(znakNL);
        }

    }

    static void c() {
        int n = 5;
        String znak = "",
                znakPrzesuniecia = " ",
                znakNL = "\n";

        for (int y = 1; y <= n; y++) {
            for (int x = 1; x <= (2 * n) - 1; x++) {
                if ((x <= n - y) || (x >= n + y)) {
                    znak = znakPrzesuniecia;
                } else if (x <= n) {
                    znak = (x + y - n) + "";
                } else if (x > n) {
                    znak = (n - x + y) + "";
                }
                o(znak);
            }
            o(znakNL);
        }

    }

    static void o(String s) {
        System.out.print(s);
    }

}
