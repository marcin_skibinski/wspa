package pl.wspa.java.lab.lab3a;

public interface IRownanie {

    void wczytaj();

    void oblicz();

    void wyswietl();

    void pokazInfo();
}
