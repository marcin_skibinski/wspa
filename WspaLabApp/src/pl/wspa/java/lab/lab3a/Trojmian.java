package pl.wspa.java.lab.lab3a;

import static java.lang.Math.sqrt;
import static pl.wspa.java.utils.Utils.inDouble;
import static pl.wspa.java.utils.Utils.p;

/**
 *
 * ax2 +bx+c=0
 */
public class Trojmian extends Dwumian {

    protected double a;

    public Trojmian() {
    }

    public Trojmian(double a, double b, double c) {
        super(b, c);
        this.a = a;
    }

    public void wczytaj() {

        this.a = inDouble("Podaj parametr a");
        super.wczytaj();

    }

    @Override
    public void oblicz() {

        sprawdzParam(a, b, c);

        double delta = delta(a, b, c);

        double wynik;
        if (delta < 0) {
            p("\nDelta < 0.");
            p("\nTo równanie nie ma rozwiązania "
                    + "\nw zbiorze liczb rzeczywistych.");
        } else if (delta == 0) {
            wynik = -b / 2 * a;
            p("\nRozwiązanie: x = " + wynik);
        } else {
            wynik = (-b + sqrt(delta)) / 2 * a;
            p("\nRozwiązanie: x1 = " + wynik);
            wynik = (-b - sqrt(delta)) / 2 * a;
            p("\n, x2 = " + wynik);
        }

    }

    private void sprawdzParam(double a, double b, double c) {
        if (a == 0) {
            throw new ArithmeticException("To nie jest równanie kwadratowe: A = 0!");
        }
    }

    private double delta(double a, double b, double c) {

        double delta = b * b - 4 * a * c;
        return delta;
    }

    @Override
    public void wyswietl() {
        p(getA() + "x^2 + " + b + "x + " + c + " = 0");
    }

    @Override
    public void pokazInfo() {
        wyswietl();
        oblicz();
    }

    /**
     * @return the a
     */
    public double getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(double a) {
        this.a = a;
    }

}
