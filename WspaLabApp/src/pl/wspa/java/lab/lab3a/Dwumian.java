package pl.wspa.java.lab.lab3a;

import static pl.wspa.java.utils.Utils.inDouble;
import static pl.wspa.java.utils.Utils.p;

/**
 * bx+c=0
 */
public class Dwumian extends AJednomian implements IRownanie {

    protected double b;

    public Dwumian() {
    }

    public Dwumian(double b, double c) {
        super(c);
        this.b = b;
    }

    @Override
    public void wczytaj() {

        this.b = inDouble("Podaj parametr b");
        this.c = inDouble("Podaj parametr c");
    }

    @Override
    public void oblicz() {

        double x;
        x = -c / b;

        //TODO: sprawdzić, czy 0 <> b
        if (Double.isNaN(x)) {
            p("\nNależy wprowadzić właściwe parametry");
        } else {
            p("\nRozwiązanie x = " + x);
        }
    }

    @Override
    public void wyswietl() {
        p(b + "x+" + c + " = 0");
    }

    @Override
    public void pokazInfo() {
        wyswietl();
        oblicz();
    }

}
