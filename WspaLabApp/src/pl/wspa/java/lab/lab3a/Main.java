package pl.wspa.java.lab.lab3a;

import static pl.wspa.java.utils.Utils.p;

public class Main {

    public static void main(String[] args) {

        Dwumian d1 = new Dwumian(1, 2);
        Dwumian d2 = new Dwumian(2, 3);
        Dwumian t1 = new Trojmian(1, 20, 4);
        Dwumian t2 = new Trojmian(4, 40, 4);

//        d1.wczytaj();
//        d1.pokazInfo();
//
//        d2.wczytaj();
//        d2.pokazInfo();
//
//        t1.wczytaj();
//        t1.pokazInfo();
//
//        t2.wczytaj();
//        t2.pokazInfo();
        Dwumian[] rownania = {d1, d2, t1, t2};
        for (Dwumian r : rownania) {
            p("\n---------------------------------------------"
                    + "\n-------------------------------------------");
            if (r instanceof Trojmian) {
                p("\nTrójmian: ");
            } else if (r instanceof Dwumian) {
                p("\nDwumian: ");
            }

            //r.wczytaj();
            r.pokazInfo();
        }
    }
}
