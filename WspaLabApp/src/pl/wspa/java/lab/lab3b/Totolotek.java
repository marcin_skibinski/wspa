package pl.wspa.java.lab.lab3b;

import static pl.wspa.java.utils.Utils.p;

public class Totolotek {

    private final int //
            MAX_LICZBA = 49,
            ILOSC_ZAKLADOW = 3,
            ILOSC_LICZB_LOSOWYCH = 6;

    private int[][] wygenerowaneLiczbyDlaZakladow
            = new int[ILOSC_ZAKLADOW][ILOSC_LICZB_LOSOWYCH];

    GeneratorLiczb generatorLiczb
            = new GeneratorLiczb(ILOSC_ZAKLADOW, ILOSC_LICZB_LOSOWYCH, MAX_LICZBA);

    public Totolotek() {
        wczytaj();
        wyswietlTabliceLiczb(wygenerowaneLiczbyDlaZakladow);
        //p(Arrays.deepToString(wygenerowaneLiczbyDlaZakladow));

    }

    //wyczytuje wygenerowane liczby do tablicy
    private void wczytaj() {
        wygenerowaneLiczbyDlaZakladow = generatorLiczb.generujTabliceLiczb();
    }

    //wyświetla wygenerowane liczby
    public void wyswietlTabliceLiczb(int[][] tabLiczb) {
        for (int i = 0; i < tabLiczb.length; i++) {
            for (int j = 0; j < tabLiczb[i].length; j++) {
                p("t[" + i + "][" + j + "]:" + tabLiczb[i][j] + "\t");
            }
            p("---------------------------------\n");
        }
    }

    public void sprawdzWygrana(int[][] zakladyUzytkownika) {

        int[] iloscTrafien = new int[wygenerowaneLiczbyDlaZakladow.length];
        int trafienie;

        //zaklad wygenerowane
        for (int i = 0; i < wygenerowaneLiczbyDlaZakladow.length; i++) {

            p("\nzakład: " + (i + 1) + " ");
            iloscTrafien[i] = 0;

            //losy wygenerowane
            for (int j = 0; j < wygenerowaneLiczbyDlaZakladow[i].length; j++) {

                for (int k = 0; k < zakladyUzytkownika[i].length; k++) {

                    if (wygenerowaneLiczbyDlaZakladow[i][j] == zakladyUzytkownika[i][k]) {
                        trafienie = wygenerowaneLiczbyDlaZakladow[i][j];
                        p("#" + trafienie + " ");
                        iloscTrafien[i]++;
                    }
                }
            }

        }
//        p("\n");
//        for (int i : iloscTrafien) {
//            p("Ilość trafień: " + iloscTrafien[i] + "\n");
//        }
        p("\n");
    }

    @Deprecated
    public void sprawdzWygrana2(int[][] zakladyUzytkownika) {
        //TODO: dorobić sprawdzenie, czy zakladyUzytkownika nie zawierają liczb spoza zakresu
        class wynikZakladu {

            int[] trafienia;

            wynikZakladu(int[] trafienia) {
                this.trafienia = trafienia;
            }
        }
        wynikZakladu[] wynikiZakladow = new wynikZakladu[ILOSC_ZAKLADOW];

        for (wynikZakladu w : wynikiZakladow) {
            int[] t = new int[ILOSC_LICZB_LOSOWYCH];
            w = new wynikZakladu(t);
        }

        //zaklad wygenerowane
        for (int i = 0; i < wygenerowaneLiczbyDlaZakladow.length; i++) {

            p("zakład: " + (i + 1));
            int nrTrafienia = 0;
            int[] traf = new int[ILOSC_LICZB_LOSOWYCH];
            //losy wygenerowane
            for (int j = 0; j < wygenerowaneLiczbyDlaZakladow[i].length; j++) {

                for (int k = 0; k < zakladyUzytkownika[i].length; k++) {

                    if (wygenerowaneLiczbyDlaZakladow[i][j] == zakladyUzytkownika[i][k]) {

                        traf[nrTrafienia] = wygenerowaneLiczbyDlaZakladow[i][j];
                        nrTrafienia++;

                        p(nrTrafienia + "#" + wygenerowaneLiczbyDlaZakladow[i][j]);
                    }
                }
            }

            wynikiZakladow[i].trafienia = new int[ILOSC_LICZB_LOSOWYCH];
            wynikiZakladow[i].trafienia = traf;
            for (int x : traf) {
                p(x);
            }
        }

//        int i = 0;
//        for (wynikZakladu w : wZ) {
//            i++;
//            p("Zakład: " + i + " " + w.trafienia.length);
//        }
    }

}
