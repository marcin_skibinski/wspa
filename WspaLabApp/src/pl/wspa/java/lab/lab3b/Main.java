package pl.wspa.java.lab.lab3b;

public class Main {

    public static void main(String[] args) {

        int[][] zakladyUzytkownika = {
            {7, 20, 8, 5, 4, 1},
            {2, 8, 5, 49, 41, 40},
            {31, 32, 33, 35, 21, 29}};

        Totolotek totolotek = new Totolotek();

        totolotek.sprawdzWygrana(zakladyUzytkownika);

    }

}
