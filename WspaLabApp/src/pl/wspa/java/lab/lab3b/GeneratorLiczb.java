package pl.wspa.java.lab.lab3b;

import java.util.Random;

public class GeneratorLiczb {

    private int //
            maxLiczba,
            iloscWierszy,
            iloscKolumn;

    private final int[][] tabLiczb
            = new int[iloscWierszy][iloscKolumn];

    public GeneratorLiczb(int iloscWierszy, int iloscKolumn, int maxLiczba) {
        this.iloscWierszy = iloscWierszy;
        this.iloscKolumn = iloscKolumn;
        this.maxLiczba = maxLiczba;
    }

    //wyczytuje wygenerowane liczby do tablicy
    private void wczytaj() {
        for (int w = 0; w < iloscWierszy; w++) {
            tabLiczb[w]
                    = generujNiepowtarzalneLiczby(iloscKolumn);
        }
    }

    public int[][] generujTabliceLiczb() {
        int[][] tabLiczb = new int[iloscWierszy][iloscKolumn];
        for (int w = 0; w < iloscWierszy; w++) {
            tabLiczb[w]
                    = generujNiepowtarzalneLiczby(iloscKolumn);
        }
        return tabLiczb;
    }

    private int[] generujNiepowtarzalneLiczby(int iloscLiczbWWierszu) {
        int[] liczbyWWierszu = new int[iloscLiczbWWierszu];
        for (int i = 0; i < iloscLiczbWWierszu; i++) {
            liczbyWWierszu[i] = genLiczbe(maxLiczba);
        }

        //generuje dopóki nie znajdzie niepowtarzalnych
        while (!czyLiczbyNiepowtarzalne(liczbyWWierszu)) {
            for (int i = 0; i < iloscLiczbWWierszu; i++) {
                liczbyWWierszu[i] = genLiczbe(maxLiczba);
            }
        }

        return liczbyWWierszu;
    }

    public static boolean czyLiczbyNiepowtarzalne(int[] tab) {

        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j <= i; j++) {
                if ((i != j) & (tab[i] == tab[j])) {
                    return false;
                }
            }
        }
        return true;
    }

    public int[] genLiczby(int iloscLiczb, int maxVal) {

        int[] liczby = new int[iloscLiczb];

        for (int i = 0; i < iloscLiczb; i++) {
            liczby[i] = genLiczbe(maxVal);
        }

        return liczby;
    }

    public int genLiczbe(int maxVal) {

        Random r;
        int liczbaLosowa;

        liczbaLosowa = new Random().nextInt(maxVal + 1);

        while (liczbaLosowa == 0) {
            liczbaLosowa = new Random().nextInt(maxVal + 1);
        }

        return liczbaLosowa;
    }

    /**
     * @return the tabLiczb
     */
    public int[][] getTabLiczb() {
        return tabLiczb;
    }

}
