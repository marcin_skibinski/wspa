package pl.wspa.java.lab.lab3b;

import java.util.Random;
import static pl.wspa.java.utils.Utils.p;

public class Totolotek1 {

    private final int //
            MAX_LICZBA = 49,
            ILOSC_ZAKLADOW = 3,
            ILOSC_LICZB_LOSOWYCH = 6;

    private final int[][] wygenerowaneLiczbyDlaZakladow
            = new int[ILOSC_ZAKLADOW][ILOSC_LICZB_LOSOWYCH];

    public Totolotek1() {

        wczytaj();
        wyswietl(wygenerowaneLiczbyDlaZakladow);

    }

    //wyczytuje wygenerowane liczby do tablicy
    private void wczytaj() {
        for (int i = 0; i < ILOSC_ZAKLADOW; i++) {
            wygenerowaneLiczbyDlaZakladow[i]
                    = generujLiczbyDlaZakladu(ILOSC_LICZB_LOSOWYCH);
        }
    }

    //wyświetla wygenerowane liczby
    private void wyswietl(int[][] wygenerowaneLiczbyDlaZakladow) {
        for (int i = 0; i < wygenerowaneLiczbyDlaZakladow.length; i++) {
            for (int j = 0; j < wygenerowaneLiczbyDlaZakladow[i].length; j++) {
                p("gen[" + i + "][" + j + "]:   " + wygenerowaneLiczbyDlaZakladow[i][j]);
            }
            p("---------------------------------\n");
        }
    }

    private int[] generujLiczbyDlaZakladu(int iloscLiczb) {
        int[] liczbyDlaZakladu = new int[iloscLiczb];
        for (int i = 0;
                i < iloscLiczb;
                i++) {
            liczbyDlaZakladu[i] = genLiczbe(MAX_LICZBA);
        }

        while (!sprawdz(liczbyDlaZakladu)) {
            for (int i = 0; i < iloscLiczb; i++) {
                liczbyDlaZakladu[i] = genLiczbe(MAX_LICZBA);
            }
        }

        return liczbyDlaZakladu;
    }

    //sprawdza niepowtarzalność
    private static boolean sprawdz(int[] tab) {

        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j <= i; j++) {
                if ((i != j) & (tab[i] == tab[j])) {
                    return false;
                }
            }
        }
        return true;
    }

    public void sprawdzWygrana1(int[][] zakladyUzytkownika) {

        int[] iloscTrafien = new int[wygenerowaneLiczbyDlaZakladow.length];

        //zaklad wygenerowane
        for (int i = 0; i < wygenerowaneLiczbyDlaZakladow.length; i++) {

            p("zakład: " + (i + 1));
            iloscTrafien[i] = 0;

            //losy wygenerowane
            for (int j = 0; j < wygenerowaneLiczbyDlaZakladow[i].length; j++) {

                for (int k = 0; k < zakladyUzytkownika[i].length; k++) {

                    if (wygenerowaneLiczbyDlaZakladow[i][j] == zakladyUzytkownika[i][k]) {
                        p("#" + wygenerowaneLiczbyDlaZakladow[i][j]);
                        iloscTrafien[i]++;
                    }
                }
            }
        }

        for (int i : iloscTrafien) {
            p(i);
        }
    }

    public void sprawdzWygrana(int[][] zakladyUzytkownika) {
        //TODO: dorobić sprawdzenie, czy zakladyUzytkownika nie zawierają liczb spoza zakresu
        class wynikZakladu {

            int[] trafienia;

            wynikZakladu(int[] trafienia) {
                this.trafienia = trafienia;
            }
        }
        wynikZakladu[] wynikiZakladow = new wynikZakladu[ILOSC_ZAKLADOW];

        for (wynikZakladu w : wynikiZakladow) {
            int[] t = new int[ILOSC_LICZB_LOSOWYCH];
            w = new wynikZakladu(t);
        }

        //zaklad wygenerowane
        for (int i = 0; i < wygenerowaneLiczbyDlaZakladow.length; i++) {

            p("zakład: " + (i + 1));
            int nrTrafienia = 0;
            int[] traf = new int[ILOSC_LICZB_LOSOWYCH];
            //losy wygenerowane
            for (int j = 0; j < wygenerowaneLiczbyDlaZakladow[i].length; j++) {

                for (int k = 0; k < zakladyUzytkownika[i].length; k++) {

                    if (wygenerowaneLiczbyDlaZakladow[i][j] == zakladyUzytkownika[i][k]) {

                        traf[nrTrafienia] = wygenerowaneLiczbyDlaZakladow[i][j];
                        nrTrafienia++;

                        p(nrTrafienia + "#" + wygenerowaneLiczbyDlaZakladow[i][j]);
                    }
                }
            }

            wynikiZakladow[i].trafienia = new int[ILOSC_LICZB_LOSOWYCH];
            wynikiZakladow[i].trafienia = traf;
            for (int x : traf) {
                p(x);
            }
        }

//        int i = 0;
//        for (wynikZakladu w : wZ) {
//            i++;
//            p("Zakład: " + i + " " + w.trafienia.length);
//        }
    }

    private int[] genLiczby(int iloscLiczb, int maxVal) {

        int[] liczby = new int[iloscLiczb];

        for (int i = 0; i < iloscLiczb; i++) {
            liczby[i] = genLiczbe(maxVal);
        }

        return liczby;
    }

    private int genLiczbe(int maxVal) {

        Random r;
        int liczbaLosowa;

        liczbaLosowa = new Random().nextInt(maxVal + 1);

        while (liczbaLosowa == 0) {
            liczbaLosowa = new Random().nextInt(maxVal + 1);
        }

        return liczbaLosowa;
    }

}
