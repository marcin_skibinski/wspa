package pl.wspa.java.lab.lab5a;

public abstract class Wzory {

    static double dodaj(double x, double y
    ) {

        return x + y;
    }

    static double odejmij(double x, double y
    ) {

        return x - y;
    }

    static double pomnoz(double x, double y
    ) {

        return x * y;
    }

    static double podziel(double x, double y
    ) {

        return x / y;
    }
}
