package pl.wspa.java.lab.lab5a;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class KalkulatorView extends JFrame {

    JLabel xLb, yLb, wynikLb;
    JTextField xTx, yTx, wynikTx;
    JButton obliczBt;
    ButtonGroup dzialaniaBG;
    JRadioButton dodawanieRB, odejmowanieRB, mnozenieRB, dzielenieRB;

    public KalkulatorView() {

        super("Kalkulator");

        setUIFont(new javax.swing.plaf.FontUIResource("Serif", Font.BOLD, 18));

        xLb = new JLabel("Podaj x:");
        xTx = new JTextField("11");
        xTx.setActionCommand("xTx");

        yLb = new JLabel("Podaj y:");
        yTx = new JTextField("33");
        yTx.setActionCommand("yTx");

        dodawanieRB = new JRadioButton("Dodawanie", true);
        odejmowanieRB = new JRadioButton("Odejmowanie");
        mnozenieRB = new JRadioButton("Mnożenie");
        dzielenieRB = new JRadioButton("Dzielenie");

        dzialaniaBG = new ButtonGroup();
        dzialaniaBG.add(dodawanieRB);
        dzialaniaBG.add(odejmowanieRB);
        dzialaniaBG.add(mnozenieRB);
        dzialaniaBG.add(dzielenieRB);

        obliczBt = new JButton("Oblicz");
        obliczBt.setActionCommand("obliczBt");

        wynikLb = new JLabel("Wynik:");
        wynikTx = new JTextField();
        wynikTx.setEditable(false);

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        p1.setLayout(new GridLayout(4, 2));
        p2.setLayout(new GridLayout());
        p3.setLayout(new GridLayout(1, 2));

        p1.add(xLb);
        p1.add(xTx);
        p1.add(yLb);
        p1.add(yTx);
        p1.add(dodawanieRB);
        p1.add(odejmowanieRB);
        p1.add(mnozenieRB);
        p1.add(dzielenieRB);

        p2.add(obliczBt);

        p3.add(wynikLb);
        p3.add(wynikTx);

        setLayout(new BorderLayout());

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);

        setMinimumSize(new Dimension(400, 150));
        pack();
        setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void setUIFont(javax.swing.plaf.FontUIResource f) {
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value != null && value instanceof javax.swing.plaf.FontUIResource) {
                UIManager.put(key, f);
            }
        }
    }

}
