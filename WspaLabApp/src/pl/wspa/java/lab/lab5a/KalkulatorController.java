package pl.wspa.java.lab.lab5a;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import static pl.wspa.java.lab.lab5a.Wzory.dodaj;
import static pl.wspa.java.lab.lab5a.Wzory.odejmij;
import static pl.wspa.java.lab.lab5a.Wzory.podziel;
import static pl.wspa.java.lab.lab5a.Wzory.pomnoz;

public class KalkulatorController extends KalkulatorView implements DocumentListener, ActionListener {

    public KalkulatorController() {

        xTx.getDocument().addDocumentListener(this);
        obliczBt.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        double x = Double.parseDouble(xTx.getText()),
                y = Double.parseDouble(yTx.getText());

        if (e.getActionCommand().equals("obliczBt")) {

            if (dodawanieRB.isSelected() == true) {

                wynikTx.setText(dodaj(x, y) + "");
            }
            if (odejmowanieRB.isSelected() == true) {
                wynikTx.setText(odejmij(x, y) + "");
            }
            if (mnozenieRB.isSelected() == true) {
                wynikTx.setText(pomnoz(x, y) + "");
            }
            if (dzielenieRB.isSelected() == true) {
                if (y == 0) {
                    wynikTx.setText("Dzielenie przez zero");
                } else {
                    wynikTx.setText(podziel(x, y) + "");
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        warn();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        warn();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void warn() {
        try {
            double x = Double.parseDouble(xTx.getText()),
                    y = Double.parseDouble(yTx.getText());
            obliczBt.setEnabled(true);
            wynikTx.setText("");

        } catch (NumberFormatException exc) {
            obliczBt.setEnabled(false);
            wynikTx.setText(" Błędny format liczby");
        }
    }

    public static void main(String[] args) {
        //Tworzenie okna w wątku rozdziału zdarzeń,
        //zamiast w głównym wątku aplikacji
        //aby uniknąć np. próby równoczesnej
        //aktualizacji komponentu przez dwa wątki
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                new KalkulatorController();
            }
        }
        );

    }

}
