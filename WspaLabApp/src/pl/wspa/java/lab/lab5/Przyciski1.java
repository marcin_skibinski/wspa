package pl.wspa.java.lab.lab5;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Przyciski1 extends JFrame implements ActionListener {
//komponenty GUI aplikacji (4 przyciski):

    JButton b1, b2, b3, b4;
    JTextField t1;
//konstruktor aplikacji (klasy Przyciski):

    public Przyciski1() {
        super("Przyciski"); //wywołanie konstruktora nadklasy (klasy JFrame)
        setLayout(new BorderLayout());

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();

        //setLayout(new FlowLayout()); //ustawienie Layoutu dla okna aplikacji
        p1.setLayout(new GridLayout());
        p2.setLayout(new GridLayout(2, 2));

        //p1.setLayout(new BorderLayout());
        //utworzenie przycisków:
        b1 = new JButton("Przycisk 1");
        b2 = new JButton("Przycisk 2");
        b3 = new JButton("Przycisk 3");
        b4 = new JButton("Przycisk 4");

        t1 = new JTextField();

        //dodanie słuchaczy akcji do przycisków:
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);

        //zablokowanie przycisku 4:
        b4.setEnabled(false);

        //dodanie komponentów do okna aplikacji (JFrame)
        p1.add(t1);

        p2.add(b1);
        p2.add(b2);
        p2.add(b3);
        p2.add(b4);

        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);

        //upakowanie komponentów i pokazanie okna aplikacji:
        pack();
        setVisible(true);
    }

//w metodzie main tworzymy jedynie nowy obiekt aplikacji za pomocą konstruktora
    public static void main(String[] args) {
        Przyciski1 ap = new Przyciski1();
    }
//obsługę zdarzeń kliknięcia na przyciski realizujemy w metodzie actionPerformed
//interfejsu ActionListener:

    @Override
    public void actionPerformed(ActionEvent e) {

        Object o = e.getSource(); //uzyskanie obiektu, na którym wykonano zdarzenie e
        if (o == b1) {
//            JOptionPane.showMessageDialog(
//                    this,
//                    "Wybrano przycisk 1",
//                    "Komunikat1",
//                    JOptionPane.INFORMATION_MESSAGE
//            );
            t1.setText("Wybrano przycisk 1");
        }
        if (o == b2) {
            JOptionPane.showMessageDialog(
                    this,
                    "Wybrano przycisk 2",
                    "Komunikat2",
                    JOptionPane.PLAIN_MESSAGE
            );
        }
        if (o == b3) {
            JOptionPane.showMessageDialog(
                    this,
                    "Wybrano przycisk 3",
                    "Komunikat3",
                    JOptionPane.WARNING_MESSAGE
            );
        }
    }
}
