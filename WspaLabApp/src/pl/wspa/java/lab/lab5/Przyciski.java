package pl.wspa.java.lab.lab5;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Przyciski extends JFrame implements ActionListener {
//komponenty GUI aplikacji (4 przyciski):

    JButton b1, b2, b3, b4;
    JTextField t1;
//konstruktor aplikacji (klasy Przyciski):

    public Przyciski() {
        super("Przyciski"); //wywołanie konstruktora nadklasy (klasy JFrame)

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        //setLayout(new FlowLayout()); //ustawienie Layoutu dla okna aplikacji
        //setLayout(new GridLayout(4, 1));
        p1.setLayout(new GridLayout(2, 1));
        p2.setLayout(new GridLayout());
        p3.setLayout(new GridLayout(2, 1));
        //p2.setLayout(new FlowLayout());

        setLayout(new GridLayout(1, 3));
        //p1.setLayout(new BorderLayout());

        //utworzenie przycisków:
        b1 = new JButton("Przycisk 1");
        b2 = new JButton("Przycisk 2");
        b3 = new JButton("Przycisk 3");
        b4 = new JButton("Przycisk 4");

        t1 = new JTextField();

        //dodanie słuchaczy akcji do przycisków:
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);

        //zablokowanie przycisku 4:
        b4.setEnabled(false);

        //dodanie komponentów do okna aplikacji (JFrame)
        p1.add(b1);
        p1.add(b2);

        p2.add(t1);

        p3.add(b3);
        p3.add(b4);

        add(p1);
        add(p2);
        add(p3);

        //upakowanie komponentów i pokazanie okna aplikacji:
        pack();
        setVisible(true);
    }

//w metodzie main tworzymy jedynie nowy obiekt aplikacji za pomocą konstruktora
    public static void main(String[] args) {
        Przyciski ap = new Przyciski();
    }
//obsługę zdarzeń kliknięcia na przyciski realizujemy w metodzie actionPerformed
//interfejsu ActionListener:

    @Override
    public void actionPerformed(ActionEvent e) {

        Object o = e.getSource(); //uzyskanie obiektu, na którym wykonano zdarzenie e
        if (o == b1) {
//            JOptionPane.showMessageDialog(
//                    this,
//                    "Wybrano przycisk 1",
//                    "Komunikat1",
//                    JOptionPane.INFORMATION_MESSAGE
//            );
            t1.setText("Wybrano przycisk 1");
        }
        if (o == b2) {
            JOptionPane.showMessageDialog(
                    this,
                    "Wybrano przycisk 2",
                    "Komunikat2",
                    JOptionPane.PLAIN_MESSAGE
            );
        }
        if (o == b3) {
            JOptionPane.showMessageDialog(
                    this,
                    "Wybrano przycisk 3",
                    "Komunikat3",
                    JOptionPane.WARNING_MESSAGE
            );
        }
    }
}
