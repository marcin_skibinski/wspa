package pl.wspa.java.lab.lab4.cw1;

import static pl.wspa.java.utils.Utils.inInt;
import static pl.wspa.java.utils.Utils.p;

public class Main {

    final static double MIN_SREDNIA = 4.1;

    public static void main(String[] args) {

        int iloscStudentow = 0;

        iloscStudentow = inInt("Podaj ilość studentów");

        Student[] studenci = new Student[iloscStudentow];

        for (int i = 0; i < iloscStudentow; i++) {
            studenci[i] = new Student();
            p("#### STUDENT " + (i + 1) + "\n");
            studenci[i].wczytaj();
        }

        int lp = 0;
        boolean jestStypenysta = false;
        for (Student s : studenci) {
            lp++;
            if (s.obliczSrednia(s.egzaminy) >= MIN_SREDNIA) {
                jestStypenysta = true;
                p("#### STUDENT " + lp + "\n");
                s.wyswietl();
            }
        }

        if (jestStypenysta == false) {
            p("Żaden ze studentów nie uzyskał średniej >= " + MIN_SREDNIA + "\n");
        }

    }

}
