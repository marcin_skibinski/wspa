package pl.wspa.java.lab.lab4.cw1;

import static pl.wspa.java.utils.Utils.inDouble;
import static pl.wspa.java.utils.Utils.inInt;
import static pl.wspa.java.utils.Utils.inString;
import static pl.wspa.java.utils.Utils.p;

public class Student extends Osoba {

    protected String numerIndeksu;
    protected int liczbaEgzaminow;
    protected Egzamin[] egzaminy;

    public Student() {
    }

    public Student(String nazwisko,
            String imie,
            String pesel,
            String numerIndeksu,
            int liczbaEgzaminow
    ) {
        super(nazwisko, imie, pesel);
        this.numerIndeksu = numerIndeksu;
        this.liczbaEgzaminow = liczbaEgzaminow;
        this.egzaminy = new Egzamin[liczbaEgzaminow];

    }

    @Override
    public void wczytaj() {
        super.wczytaj();
        this.numerIndeksu = inString("Numer indeksu");
        this.liczbaEgzaminow = inInt("Liczba egzaminów");

        this.egzaminy = new Egzamin[liczbaEgzaminow];

        for (int i = 0; i < liczbaEgzaminow; i++) {
            {
                p("Egzamin nr: " + (i + 1) + "\n");
                this.egzaminy[i] = new Egzamin(
                        inString("Przedmiot"),
                        inDouble("Ocena"));
            }

        }
    }

    double obliczSrednia(Egzamin[] egzaminy) {
        double sumaOcen = 0;

        for (Egzamin egzamin : egzaminy) {
            sumaOcen = sumaOcen + egzamin.getOcena();
        }
        return sumaOcen / egzaminy.length;
    }

    @Override
    public void wyswietl() {

        p("--------------------     WYNIKI     -------------------\n");
        p("-------------------------------------------------------\n");
        super.wyswietl();
        p(
                "Numer indeksu: " + numerIndeksu + "\t"
                + "Liczba egzaminów: " + liczbaEgzaminow + "\n"
                + "Egzaminy: \n"
        );
        for (int i = 0; i < liczbaEgzaminow; i++) {
            {
                p("\tEgzamin nr: " + (i + 1) + ", "
                        + this.egzaminy[i].getNazwa() + ", "
                        + this.egzaminy[i].getOcena() + "\n"
                );
            }
        }
        p("Średnia ocen: " + obliczSrednia(this.egzaminy) + "\n");

    }

}
