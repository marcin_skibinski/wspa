package pl.wspa.java.lab.lab5b;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JPanel;

public class Obraz extends JPanel {

    Image im;
    private String nazwa;// = "obraz10.JPG";//nazwa obrazka do wyświetlenia

    Obraz(String n) {
        this.nazwa = n;
    }

    Obraz() {
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        System.out.println("### " + nazwa);
        im = Toolkit.getDefaultToolkit().getImage(nazwa);
        g.drawImage(im, 10, 10, this);
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    /**
     * @return the nazwa
     */
    public String getNazwa() {
        return nazwa;
    }

}
