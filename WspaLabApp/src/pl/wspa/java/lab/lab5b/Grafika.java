package pl.wspa.java.lab.lab5b;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Grafika extends JFrame implements ActionListener, MouseListener {

    Obraz oSmall, oBig;
    JComboBox cb;
    JFrame jfBig;

    Grafika() {
        super("Grafika w aplikacji");
        setLayout(new BorderLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(250, 200);

        cb = new JComboBox();
        for (int i = 1; i <= 10; i++) {
            cb.addItem("obraz" + i + ".JPG");
        }

        cb.setActionCommand("cb");
        cb.addActionListener(this);

        add(cb, BorderLayout.NORTH);

        //tworzymy panel z rysunkiem:
        oSmall = new Obraz("obraz1.JPG");

        //ustawiamy podwójne buforowanie:
        oSmall.setDoubleBuffered(true);

        oSmall.addMouseListener(this);

        //dodajemy do aplikacji panel z rysunkiem
        //tak samo jak każdy inny komponent:
        add(oSmall, BorderLayout.CENTER);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("cb")) {
            oSmall.setNazwa(cb.getSelectedItem().toString());
            oSmall.repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //utwórz nową ramkę JFrame
        if (jfBig == null) {
            jfBig = new JFrame("Duży format");
            jfBig.setSize(800, 600);
            jfBig.setLayout(new BorderLayout());
        }

        if (oBig == null) {
            oBig = new Obraz();
        }

        //dodaj do niej panel z dużym zdjęciem
        oBig.setNazwa("Big//" + cb.getSelectedItem().toString());
        oBig.repaint();

        jfBig.add(oBig);

        //pokaż nowo utworzoną ramkę
        jfBig.setVisible(true);

    }

    @Override
    public void mousePressed(MouseEvent e
    ) {

    }

    @Override
    public void mouseReleased(MouseEvent e
    ) {

    }

    @Override
    public void mouseEntered(MouseEvent e
    ) {
    }

    @Override
    public void mouseExited(MouseEvent e
    ) {
    }

    //definujemy pomocniczą klasę zagnieżdżoną (nested class):
    class Rysunek extends JPanel {

        @Override //nadpisujemy metodę paintComponent klasy JPanel:
        public void paintComponent(Graphics g) { //rysujemy na obiekcie g:
            g.setColor(Color.BLUE);
            g.drawString("Rysowanie na panelu", 60, 30);
            g.setColor(Color.YELLOW);
            g.fillOval(60, 50, 80, 60);
        }
    }

    public static void main(String[] args) {
        Grafika a = new Grafika();
    }
}
