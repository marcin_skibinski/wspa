package pl.wspa.java.lab.lab2;

import static pl.wspa.java.lab.lab2.Fraction.f;

public class FractionApp {

    public static void main(String[] args) {

        f(2, 3).add(3, 4).show();
        f(2, 3).subtract(3, 4).show();
        f(2, 3).multiply(3, 4).show();
        f(2, 3).divide(3, 4).show();

        (f(2, 3).add(3, 4)).divide(5, 6).show();
        (f(2, 3).multiply(3, 4)).add(5, 6).show();

        Fraction f = new Fraction(1, 1);
        f.getGreater(f(2, 3), f(5, 16)).show();

        f.getSmaller(f(1, 30), f(5, 16)).show();

        f(-120, -133 - 0).shortFraction().show();

    }
}
