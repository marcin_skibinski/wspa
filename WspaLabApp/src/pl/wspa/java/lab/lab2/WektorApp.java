package pl.wspa.java.lab.lab2;

public class WektorApp {

    public static void main(String[] args) {

        Wektor a = new Wektor("a", 1, 2, 3);
        Wektor b = new Wektor("b", 1, 1, 1);
        Wektor c = new Wektor("c", -1, 0, -10);
        Wektor d = new Wektor("d");

        a.pokaz();
        b.pokaz();
        c.pokaz();
        d.pokaz();

        d = d.dodajWektory(a, b).odejmijWektor(c);
        d.pokaz();

        System.out.println("długość: " + d.obliczDlugoscWektora());
        System.out.println("iloczyn: " + b.iloczynSkalarnyWektorow(a));
    }
}
