/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab2;

public class WektorN {

    private int n = 3;
    private String nazwa = "w";
    private double x[];

    public WektorN(String nazwa) {
        this.nazwa = nazwa;
        x = new double[n]; //zagwarantowana inicjalizacja tablicy zerami
    }

    public WektorN(String nazwa, double t[]) {
        this.nazwa = nazwa;
        n = t.length;
        x = new double[n];
        x = t.clone(); //skopiowanie wartości z tablicy t do tablicy x
    }

    void pokaz() {
        String s = "";
        for (double d : x) {
            s = s + d + ", ";
        }
        s = s.substring(0, s.length() - 2);
        System.out.print("\n" + nazwa + "[" + s + "]");
    }

    double obliczDlugoscWektora() {
        double tmp = 0;
        for (double d : x) {
            tmp = tmp + d * d;
        }
        return Math.sqrt(tmp);
    }

    WektorN dodajWektor(WektorN wektorN) {
        for (int i = 0; i < wektorN.x.length; i++) {
            this.x[i] = this.x[i] + wektorN.x[i];
        }
        return this;
    }

    WektorN OdejminWektor(WektorN wektorN) {
        for (int i = 0; i < wektorN.x.length; i++) {
            this.x[i] = this.x[i] - wektorN.x[i];
        }
        return this;
    }

    double iloczynSkalarnyWektorow(WektorN wektorN) {
        double tmp = 0;
        for (int i = 0; i < wektorN.x.length; i++) {
            tmp = tmp + this.x[i] * wektorN.x[i];
        }

        return tmp;

    }
}
