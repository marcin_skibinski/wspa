package pl.wspa.java.lab.lab2;

public class Fraction {

    private int numerator = 1/*licznik*/,
            denumerator = 1/*mianownik*/;

    public Fraction(int numerator, int denumerator) {
        this.numerator = numerator;
        this.denumerator = denumerator == 0 ? 1 : denumerator;
    }

    public static Fraction f(int numerator, int denumerator) {
        return new Fraction(numerator, denumerator);
    }

    public void show() {
        System.err.println(numerator + "/" + denumerator);
    }

    public Fraction add(int numerator, int denumerator) {

        this.numerator = this.numerator * denumerator + numerator * this.denumerator;
        this.denumerator = this.denumerator * denumerator;

        return this;
    }

    public Fraction subtract(int numerator, int denumerator) {
        this.numerator = this.numerator * denumerator - numerator * this.denumerator;
        this.denumerator = this.denumerator * denumerator;
        return this;
    }

    public Fraction multiply(int numerator, int denumerator) {
        this.numerator = this.numerator * numerator;
        this.denumerator = this.denumerator * denumerator;
        return this;
    }

    public Fraction divide(int numerator, int denumerator) {
        this.numerator = this.numerator * denumerator;
        this.denumerator = this.denumerator * numerator;
        return this;
    }

    public Fraction getGreater(Fraction f1, Fraction f2) {
        double d1 = (f1.numerator / (double) f1.denumerator),
                d2 = (f2.numerator / (double) f2.denumerator);

        if (Math.max(d1, d2) == d1) {
            return f1;
        } else {
            return f2;
        }
    }

    public Fraction getSmaller(Fraction f1, Fraction f2) {
        double d1 = (f1.numerator / (double) f1.denumerator),
                d2 = (f2.numerator / (double) f2.denumerator);

        if (Math.min(d1, d2) == d1) {
            return f1;
        } else {
            return f2;
        }
    }

    public Fraction shortFraction() {
        int a, b, c, l, m;
        l = this.numerator;
        m = this.denumerator;

        if (l <= 0 || m <= 0) {
            throw new UnsupportedOperationException("To nie jest poprawny ulamek'");
        } else {

            a = l;
            b = m;

            // Algorytm Euklidesa
            while (a != b) {
                if (a > b) {
                    a = a - b;
                } else {
                    b = b - a;
                }
            }

            //System.out.println("NWD(" + l + "," + m + ") = " + a);
            a = l / a;
            b = m / b;
            c = a / b;
            a = a % b;

            if (c == 0) {
                this.numerator = a;
                this.denumerator = b;
            } else {
                if (a == 0) {
                    this.numerator = c;
                    this.denumerator = 1;
                } else {
                    this.numerator = a;
                    this.denumerator = b;
                }
            }
        }
        return this;
    }
}
