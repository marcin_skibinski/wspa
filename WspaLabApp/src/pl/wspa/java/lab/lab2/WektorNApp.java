package pl.wspa.java.lab.lab2;

public class WektorNApp {

    public static void main(String[] args) {

        WektorN z = new WektorN("z"); //x[0,0,0]

        double tx[] = {-12, 23, 34, 55, 767, 87, 88, 8889};
        WektorN x = new WektorN("x", tx);

        double ty[] = {2, 3, 4, 5, 6, 7, 8, 9};
        WektorN y = new WektorN("y", ty);

        x.pokaz();
        y.pokaz();
        System.out.println();
        x = x.dodajWektor(y);
        x.pokaz();
        x = x.OdejminWektor(y);
        x.pokaz();
        System.out.println();

        System.out.println("długość y: " + y.obliczDlugoscWektora());
        System.out.println("iloczyn xy: " + x.iloczynSkalarnyWektorow(y));
    }
}
