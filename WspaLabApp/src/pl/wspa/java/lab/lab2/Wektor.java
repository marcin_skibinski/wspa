package pl.wspa.java.lab.lab2;

/**
 * Klasa Wektor reprezentuje wektor z przestrzeni 3D o trzech współrzędnych
 * x,y,z i udostępnia metody realizujące różne działania na wektorach
 *
*/
public class Wektor {

    protected String nazwa = "w"; //pole nazwa - domniemana wartość "w"
    protected double x = 0; //współrzędna wektora x o wartości domniemanej 0
    protected double y = 0; //współrzędna wektora y o wartości domniemanej 0
    protected double z = 0; //współrzędna wektora z o wartości domniemanej 0

    /**
     * Wektor() - konstruktor bezparametrowy tworzy obiekt z domniemanymi
     * wartościami atrybutów czyli wektor: w[0,0,0]
     */
    Wektor() {
    }

    /**
     * Wektor(String n) - konstruktor z 1 parametrem typu String tworzy obiekt z
     * domniemanymi wartościami współrzędnych: [0,0,0] ale nazwa musi być podana
     * jako parametr konstruktora np. Wektor w1=new Wektor("w1");
     */
    Wektor(String nazwa) {
        this.nazwa = nazwa;
    }

    Wektor(String nazwa, double x, double y, double z) {
        this.nazwa = nazwa;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Metoda pokaz() - wyświetla wektor w postaci w[x,y,z]
     */
    void pokaz() {
        System.out.print("\n" + nazwa + "[" + x + "," + y + "," + z + "]");
    }

    /**
     * Metoda oblicz_dlugosc()
     *
     * @return - zwraca długość wektora o danych współrzędnych
     */
    double obliczDlugoscWektora() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Metoda dodaj_wektory - dodaje wektor pokazywany autoreferencją this i
     * wektor a przekazany jako parametr metody
     *
     * @param a - Wektor
     * @return - Wektor będący sumą wektora this i a
     */
    Wektor dodajWektor(Wektor a) {

        this.x = this.x + a.x;
        this.y = this.y + a.y;
        this.z = this.z + a.z;
        return this; //wynikiem jest obiekt Wektor
    }

    Wektor dodajWektory(Wektor a, Wektor b) {

        this.x = a.x + b.x;
        this.y = a.y + b.y;
        this.z = a.z + b.z;
        return this; //wynikiem jest obiekt Wektor
    }

    Wektor odejmijWektor(Wektor a) {
        this.x = this.x - a.x;
        this.y = this.y - a.y;
        this.z = this.z - a.z;
        return this; //wynikiem jest obiekt Wektor
    }

    Wektor odejmijWektory(Wektor a, Wektor b) {

        this.x = a.x - b.x;
        this.y = a.y - b.y;
        this.z = a.z - b.z;
        return this; //wynikiem jest obiekt Wektor
    }

    //dodaj metodę iloczyn_skalarny_wektorów
    double iloczynSkalarnyWektorow(Wektor a) {

        return this.x * a.x + this.y * a.y + this.z * a.z;

    }

}
