package pl.wspa.java.lab.lab3.cw4;

import static pl.wspa.java.utils.Utils.inString;
import static pl.wspa.java.utils.Utils.p;

public class Osoba implements IBaza {

    protected String nazwisko, imie, pesel;

    public Osoba() {
    }

    public Osoba(String nazwisko, String imie, String pesel) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.pesel = pesel;
    }

    @Override
    public void wczytaj() {

        this.imie = inString("Imię");
        this.nazwisko = inString("Nazwisko");
        this.pesel = inString("PESEL");
    }

    @Override
    public void wyswietl() {
        p(
                nazwisko + "\t"
                + imie + "\t"
                + pesel + "\t"
        );
    }

}
