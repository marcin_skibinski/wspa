package pl.wspa.java.lab.lab3.cw4;

import static pl.wspa.java.utils.Utils.p;

public class Main {

    final static double MIN_SREDNIA = 4.1;

    public static void main(String[] args) {
        Student[] studenci = {
            new Student(),
            new Student(),
            new Student()
        };

//        Osoba o = new Osoba("Ąckiński", "Ęcek", "12345678901");
//        o.p();
//
//        o = new Osoba();
//        o.wczytaj(); o.p();
        int lp = 0;
        for (Student s : studenci) {
            lp++;
            p("#### STUDENT " + lp + "\n");
            s.wczytaj();
            //s.p();
        }

        lp = 0;
        for (Student s : studenci) {
            if (s.obliczSrednia(s.egzaminy) >= MIN_SREDNIA) {
                lp++;
                p("#### STUDENT " + lp + "\n");
                s.wyswietl();
            }
        }

    }

}
