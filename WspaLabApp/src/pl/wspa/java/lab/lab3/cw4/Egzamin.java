package pl.wspa.java.lab.lab3.cw4;

public class Egzamin {

    private String nazwaPrzedmiotu;
    private double ocena;

    public Egzamin(String nazwaPrzedmiotu, double ocena) {
        this.nazwaPrzedmiotu = nazwaPrzedmiotu;
        this.ocena = ocena;
    }

    /**
     * @return the nazwaPrzedmiotu
     */
    public String getNazwa() {
        return nazwaPrzedmiotu;
    }

    /**
     * @param nazwa the nazwaPrzedmiotu to set
     */
    public void setNazwa(String nazwa) {
        this.nazwaPrzedmiotu = nazwa;
    }

    /**
     * @return the ocena
     */
    public double getOcena() {
        return ocena;
    }

    /**
     * @param ocena the ocena to set
     */
    public void setOcena(double ocena) {
        this.ocena = ocena;
    }

}
