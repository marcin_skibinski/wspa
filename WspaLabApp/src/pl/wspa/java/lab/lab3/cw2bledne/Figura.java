/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2bledne;

public class Figura {

    //powinno być private
    protected String nazwa = "Figura geometryczna";

    //nie potrzebny
    Figura() {
    }

    Figura(String nazwa) {
        this.nazwa = nazwa;
    }

    //dla czytelności
    private double pole() {
        return 0;
    }

    //nie potrzebny public
    public void wyswietl() {
        System.out.print("\n" + nazwa + ", pole [figury]: " + pole());
    }
}
