/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2bledne;

public class Prostokat extends Kwadrat {

    private final double bokB;

    Prostokat(String nazwa, double bokA, double bokB) {
        super(nazwa, bokA);
        this.bokB = bokB;
    }

    public double getBokB() {
        return bokB;
    }

    //private
    double pole() {
        return this.getBokA() * this.getBokB();
    }

    @Override
    //nadpisanie (przesłonięcie) metody klasy bazowej
    public void wyswietl() {
        super.wyswietl();
        System.out.print(", bok b = " + this.getBokB()
                + ", pole [prostokąta]: " + this.pole());
    }

}
