/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2bledne;

public class Prostopadloscian extends Prostokat {

    private final double bokC;

    Prostopadloscian(String nazwa, double bokA, double bokB, double bokC) {
        super(nazwa, bokA, bokB);
        this.bokC = bokC;
    }

    //private
    double pole() {
        return 2 * this.getBokA() * this.getBokB() + 2 * this.bokC * (this.getBokA() * this.getBokB());
    }

    private double objetosc() {
        return this.getBokA() * this.getBokB() * this.bokC;
    }

    @Override
    public void wyswietl() { //nadpisanie (przesłonięcie) metody klasy bazowej
        super.wyswietl();
        System.out.print(
                ", bok c =" + this.bokC
                + ", pole [prostopadłościanu]: " + pole()
                + ", objętość [prostopadłościanu]: " + objetosc());

    }
}
