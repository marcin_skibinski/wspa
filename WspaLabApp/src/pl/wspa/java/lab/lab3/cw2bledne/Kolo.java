/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2bledne;

import static java.lang.Math.PI;

public class Kolo extends Figura {

    // powinno być private
    protected double r;

    //nie trzeba definiować
    Kolo() {
        super(); //skorzystanie z bezparametrowego konstruktora nadklasy
    }

    Kolo(String nazwa, double r) {
        super(nazwa);
        this.r = r;
    }

    //powinno być private
    double pole() { //nowa metoda klasy
        return PI * Math.pow(this.r, 2);
    }

    @Override
    //nie potrzebny public
    public void wyswietl() { //nadpisanie (przesłonięcie) metody klasy bazowej
        super.wyswietl();
        System.out.print(
                ", promień r = " + r
                + ", pole [koła]: " + pole());
    }
}
