/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2bledne;

public class Main {

    public static void main(String[] args) {

        Figura[] figury = {
            //            new Figura("Figurka 1"),
            //            new Kwadrat("Kwadrat 1", 11),
            //            new Kwadrat("Kwadrat 2", 22),
            new Prostokat("Prostokat 1", 1, 2) //,
        //            new Prostokat("Prostokat 2", 10, 20),
        //            new Szescian("Sześcian 1", 2),
        //            new Szescian("Sześcian 2", 1),
        //            new Prostopadloscian("Prostopadloscian 1", 5, 10, 2),
        //            new Prostopadloscian("Prostopadloscian 2", 12, 13, 1)
        };

        for (Figura f : figury) {
            f.wyswietl();
        }

        System.out.println();
        System.out.println();
    }

}
