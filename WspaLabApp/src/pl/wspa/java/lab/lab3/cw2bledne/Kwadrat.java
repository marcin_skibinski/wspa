/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2bledne;

public class Kwadrat extends Figura {

    private final double bokA;

    Kwadrat(String nazwa, double bokA) {
        super(nazwa);
        this.bokA = bokA;
    }

    public double getBokA() {
        return bokA;
    }

    //powinno być private
    double pole() {
        return Math.pow(getBokA(), 2);
    }

    @Override
    //nie potrzebny public
    public void wyswietl() { //nadpisanie (przesłonięcie) metody klasy bazowej
        super.wyswietl();
        System.out.print(", bok a = " + this.getBokA()
                + ", pole [kwadratu]: " + pole());
    }

}
