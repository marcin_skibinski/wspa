/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw1;

public class Main {

    static Figura f2;
    static Kolo k1, k2;

    public static void main(String[] args) {

        Figura f1 = new Figura();
        f2 = new Figura("Figurka 2");
        k1 = new Kolo();
        k2 = new Kolo("Kolo1", 123);
        f1.wyswietl();
        k2.wyswietl();

        System.out.print("\n\n");

    }

}
