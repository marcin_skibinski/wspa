/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw1;

class Figura {

    //powinno być private
    protected String nazwa = "Figura geometryczna";

    //nie potrzebne
    public Figura() {
    }

    Figura(String nazwa) {
        this.nazwa = nazwa;
    }

    void wyswietl() {
        System.out.print("\n" + nazwa);
    }
}
