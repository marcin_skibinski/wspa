/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw1;

import static java.lang.Math.PI;

class Kolo extends Figura {

    //private
    protected double r = 1;

    //nie potrzebne
    Kolo() {
        super(); //skorzystanie z bezparametrowego konstruktora nadklasy
    }

    Kolo(String nazwa, double r) {
        super(nazwa); //skorzystanie z konstruktora nadklasy z parametrem
        this.r = r;

    }

    double pole() { //nowa metoda klasy
        return PI * Math.pow(this.r, 2);
    }

    @Override
    public void wyswietl() { //nadpisanie (przesłonięcie) metody klasy bazowej
        super.wyswietl();
        System.out.print(
                ", promień r=" + r
                + ", pole [koła]:" + pole());
    }
}
