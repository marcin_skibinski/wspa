package pl.wspa.java.lab.lab3.cw3;

import static pl.wspa.java.utils.Utils.p;

abstract class AFigura {

    private String nazwa = "Figura geometryczna";

    AFigura() {
        this.nazwa = "Figura";
    }

    AFigura(String nazwa) {
        this.nazwa = nazwa;
    }

    void wyswietlNazwe() {
        p("\n" + this.nazwa);
    }

}
