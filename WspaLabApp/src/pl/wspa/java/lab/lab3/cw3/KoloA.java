package pl.wspa.java.lab.lab3.cw3;

import static java.lang.Math.PI;
import static pl.wspa.java.utils.Utils.p;

public class KoloA extends AFigura implements IFigura {

    private final double r;

    KoloA(String nazwa, double r) {
        super(nazwa);
        this.r = r;
    }

    @Override
    public double obliczPole() {
        return PI * this.r * this.r;
    }

    @Override
    public double obliczObwod() {
        return 2 * PI * this.r;
    }

    @Override
    public void wyswietlPole() {
        p(
                ", promień r = " + r
                + ", pole [koła]: " + obliczPole());
    }

    @Override
    public void wyswietlObwod() {
        p(", obwód [koła]: " + obliczObwod());
    }

}
