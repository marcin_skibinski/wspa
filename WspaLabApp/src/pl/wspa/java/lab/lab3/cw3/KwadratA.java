package pl.wspa.java.lab.lab3.cw3;

import static pl.wspa.java.utils.Utils.p;

public class KwadratA extends AFigura implements IFigura {

    private double bok;

    public KwadratA(String nazwa, double bok) {
        super(nazwa);
        this.bok = bok;
    }

    @Override
    public double obliczPole() {
        return this.bok * this.bok;
    }

    @Override
    public double obliczObwod() {
        return 4 * this.bok;
    }

    @Override
    public void wyswietlPole() {
        p(
                ", bok a = " + bok
                + ", pole [kwadratu]: " + obliczPole());
    }

    @Override
    public void wyswietlObwod() {

        p(", obwód [koła]: " + obliczObwod());
    }

}
