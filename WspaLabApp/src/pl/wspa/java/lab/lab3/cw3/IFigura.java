package pl.wspa.java.lab.lab3.cw3;

interface IFigura {

    double obliczPole();

    double obliczObwod();

    void wyswietlPole();

    void wyswietlObwod();

}
