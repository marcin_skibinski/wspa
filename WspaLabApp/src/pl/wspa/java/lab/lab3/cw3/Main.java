package pl.wspa.java.lab.lab3.cw3;

import static pl.wspa.java.utils.Utils.p;

public class Main {

    public static void main(String[] args) {

        KoloA koloA1 = new KoloA("Kolo 1", 10);
        KoloA koloA2 = new KoloA("Kolo 2", 1);
        KwadratA kwadratA1 = new KwadratA("Kwadrat1 1", 10);
        KwadratA kwadratA2 = new KwadratA("Kwadrat1 2", 1);

        koloA1.wyswietlNazwe();
        koloA1.wyswietlPole();
        koloA1.wyswietlObwod();

        koloA2.wyswietlNazwe();
        koloA2.wyswietlPole();
        koloA2.wyswietlObwod();

        kwadratA1.wyswietlNazwe();
        kwadratA1.wyswietlPole();
        kwadratA1.wyswietlObwod();

        kwadratA2.wyswietlNazwe();
        kwadratA2.wyswietlPole();
        kwadratA2.wyswietlObwod();

        p("\n");

    }

}
