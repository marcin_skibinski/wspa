/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2poprawne;

public class Szescian extends Kwadrat {

    Szescian(String nazwa, double bokA) {
        super(nazwa, bokA); //skorzystanie z konstruktora nadklasy z parametrem
    }

    //lepiej private
    private double pole() {
        return Math.pow(getBokA(), 2);
    }

    private double objetosc() {
        return pole() * getBokA();
    }

    @Override
    protected void wyswietl() { //nadpisanie (przesłonięcie) metody klasy bazowej
        super.wyswietl();
        System.out.print(
                ", pole [sześcianu]: " + pole()
                + ", objętość [sześcianu]: " + objetosc());
    }
}
