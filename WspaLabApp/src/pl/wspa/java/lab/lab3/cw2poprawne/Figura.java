/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wspa.java.lab.lab3.cw2poprawne;

public class Figura {

    //powinno być private
    private String nazwa = "Figura geometryczna";

    Figura(String nazwa) {
        this.nazwa = nazwa;
    }

    //dla czytelności
    private double pole() {
        return 0;
    }

    //nie potrzebny public
    protected void wyswietl() {
        System.out.print("\n" + nazwa + ", pole [figury]: " + pole());
    }
}
